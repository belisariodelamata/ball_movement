/*
 * frmGolpe.java
 *
 */
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.LinkedList;
import javax.swing.JLabel;
import javax.swing.Timer;

/**
 *
 * @author BS
 */
public class FrmPrincipal extends javax.swing.JFrame implements ActionListener {

    Timer tiempo = null;
    LinkedList<Esfera> controles = new LinkedList<>();
    int delayTimer = 20;
    //y=mx+c
    //x=(y-c)/m
    MouseListener eventosEsferas = null;
    boolean golpeSuperiorInferior = false;

    private void establecerEventos(JLabel control) {
        control.addMouseListener(eventosEsferas);
    }

    private void controlMouseClick(java.awt.event.MouseEvent evt) {
        if (evt.isShiftDown()) {
            eliminarEsfera(new Esfera((JLabel) evt.getSource()));
        } else {
            controlMouseEnter(evt);
        }
    }

    private void eliminarEsfera(Esfera esfera) {
        controles.remove(esfera);
        esfera.controlVisual.setVisible(false);
        getContentPane().remove(esfera.controlVisual);
        this.pnlContainer.repaint();
    }

    private void controlMouseEnter(java.awt.event.MouseEvent evt) {
        double x2 = (double) (((JLabel) (evt.getSource())).getWidth() / 2);
        double y2 = (double) (((JLabel) (evt.getSource())).getHeight() / 2);
        Esfera es = buscarControl((JLabel) (evt.getSource()));
        if (es != null) {

            es.setM((double) ((y2 - evt.getY()) / (x2 - evt.getX())));

            if (es.movimientoConRespectoX) {
                if (evt.getX() > es.controlVisual.getWidth() / 2) {
                    es.cambiarADireccionNegativa();
                } else {
                    es.cambiarADireccionPositiva();
                }
            } else {
                if (evt.getY() > es.controlVisual.getHeight() / 2) {
                    es.cambiarADireccionNegativa();
                } else {
                    es.cambiarADireccionPositiva();
                }
            }
            es.recalcularAPartirDeControl();
        }
    }

    private void agregarEsfera(JLabel esfera) {
        agregarEsfera(esfera, 45);
    }

    /**
     * Agregar una Esfera con un Angulo Inicial
     *
     * @param esfera
     * @param angulo
     */
    private void agregarEsfera(JLabel esfera, int angulo) {
        Esfera es = new Esfera(esfera);
        es.setAngulo(angulo);
        es.asignarPosicionAPartirDeControl();
        controles.add(es);
        establecerEventos(esfera);

    }

    private void ubicarEcuacion(Esfera es) {
        sepInterseccion.setBounds(0, es.getC(),
                sepInterseccion.getWidth(), sepInterseccion.getHeight());

        lblC.setBounds(0, es.getC(),
                lblC.getWidth(), lblC.getHeight());
        lblC.setText(es.obtenerEcuacion());

    }

    /**
     * Creates new form frmGolpe
     */
    public FrmPrincipal() {
        initComponents();
        this.setTitle("Movimiento de Objetos - Ecuación de la recta");
        sldVelocidad.setValue(delayTimer);
        Esfera.imagen = new javax.swing.ImageIcon(getClass().getResource("/image.png"));
        eventosEsferas = new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                controlMouseClick(evt);
            }

            @Override
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                //controlMouseEnter(evt);
            }
        };
        agregarEsfera(lblEsfera);
        tiempo = new Timer(sldVelocidad.getValue(), this);

    }

    public void actionPerformed(ActionEvent e) {
        for (Esfera es : controles) {
            Point siguientePosicion = es.calcularSiguientePosicion();
            //Si la siguiente posicion es de choque, no la asigna
            if (chkChocar.isSelected() && comprobarChoqueEsfera(es, siguientePosicion)) {
                if (golpeSuperiorInferior) {
                    es.golpeEnY();
                } else {
                    es.golpeEnX();
                }
                es.sigImgRotada();
            } else {//Si no es de choque entonces si la asigna
                es.asignarPosicion(siguientePosicion);
                es.sigImgRotada();
            }

            if (es.estaEnTopeDerecho(pnlContainer.getWidth())) {
                es.asignarPuntoDerecho(pnlContainer.getWidth());
                es.golpeEnX();
            } else if (es.estaEnTopeInferior(pnlContainer.getHeight())) {
                es.asignarPuntoInferior(pnlContainer.getHeight());
                es.golpeEnY();
            } else if (es.estaEnTopeIzquierdo()) {
                es.asignarPuntoIzquierdo();
                es.golpeEnX();
            } else if (es.estaEnTopeSuperior()) {
                es.asignarTopeSuperior();
                es.golpeEnY();
            }

            ubicarEcuacion(es);
        }
    }

    /**
     * Comprueba si la nueva posicion del objeto se ubica en la intersección con
     * otro objeto
     *
     * @param esfera
     * @param sigPos Coordenadas de la siguiente Posicion
     * @return
     */
    public boolean comprobarChoqueEsfera(Esfera esfera, Point sigPos) {
        JLabel esferaControl = esfera.controlVisual;
        for (Esfera esferaComparacion : controles) {
            //Se deben recorrer todas las esferas
            //para verificar si en el siguiente movimiento chocará con alguna
            if (esferaComparacion.controlVisual != esferaControl) {
                if (esfera.ultimoGolpe == null) {
                    if (esfera.hayImpacto(sigPos, esferaComparacion)) {
                        esfera.ultimoGolpe = esferaComparacion;
                        double anguloDeImpacto=esfera.anguloDeImpacto(esferaComparacion);
                        golpeSuperiorInferior =  anguloDeImpacto>= 45;
                        return true;
                    }
                }

            }
        }
        esfera.ultimoGolpe = null;
        golpeSuperiorInferior = false;
        return false;
    }

    /**
     *
     * @param control
     * @return Retorna el Objeto Esfera equivalente al Objeto visual
     */
    Esfera buscarControl(JLabel control) {
        for (Esfera esferaMov : controles) {
            if (esferaMov.controlVisual == control) {
                return esferaMov;
            }
        }
        return null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnTemporizador = new javax.swing.JButton();
        sldVelocidad = new javax.swing.JSlider();
        lblAngulo = new javax.swing.JLabel();
        lblVelocidad = new javax.swing.JLabel();
        sldAngulo = new javax.swing.JSlider();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        pnlContainer = new javax.swing.JPanel();
        lblEsfera = new javax.swing.JLabel();
        sepInterseccion = new javax.swing.JSeparator();
        lblC = new javax.swing.JLabel();
        lblAyuda = new javax.swing.JLabel();
        chkChocar = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(null);

        btnTemporizador.setText("Iniciar");
        btnTemporizador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTemporizadorActionPerformed(evt);
            }
        });
        getContentPane().add(btnTemporizador);
        btnTemporizador.setBounds(320, 30, 120, 30);

        sldVelocidad.setMinimum(60);
        sldVelocidad.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sldVelocidadStateChanged(evt);
            }
        });
        getContentPane().add(sldVelocidad);
        sldVelocidad.setBounds(110, 40, 200, 26);

        lblAngulo.setText("Ángulo");
        getContentPane().add(lblAngulo);
        lblAngulo.setBounds(10, 80, 90, 20);

        lblVelocidad.setText("Velocidad");
        getContentPane().add(lblVelocidad);
        lblVelocidad.setBounds(10, 40, 90, 20);

        sldAngulo.setMaximum(360);
        sldAngulo.setValue(45);
        sldAngulo.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sldAnguloStateChanged(evt);
            }
        });
        getContentPane().add(sldAngulo);
        sldAngulo.setBounds(110, 80, 200, 26);

        jLabel1.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jLabel1.setText("y= mx + c");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(10, 0, 170, 30);
        getContentPane().add(jSeparator1);
        jSeparator1.setBounds(10, 110, 760, 10);

        pnlContainer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));
        pnlContainer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pnlContainerMouseClicked(evt);
            }
        });
        pnlContainer.setLayout(null);

        lblEsfera.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image.png"))); // NOI18N
        lblEsfera.setAlignmentY(0.0F);
        pnlContainer.add(lblEsfera);
        lblEsfera.setBounds(0, 0, 40, 40);
        pnlContainer.add(sepInterseccion);
        sepInterseccion.setBounds(0, 180, 370, 30);

        lblC.setText("--");
        lblC.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        pnlContainer.add(lblC);
        lblC.setBounds(0, 180, 380, 14);

        getContentPane().add(pnlContainer);
        pnlContainer.setBounds(10, 120, 760, 380);

        lblAyuda.setText("<html>\n<b>Comandos:</b> <br>\nClick para agregar esfera.<br>\nShift+Click sobre esfera para eliminarla.<br>\nClick sobre esfera para cambiar su ángulo.\n</html>");
        getContentPane().add(lblAyuda);
        lblAyuda.setBounds(450, 0, 310, 110);

        chkChocar.setSelected(true);
        chkChocar.setText("Chocar");
        getContentPane().add(chkChocar);
        chkChocar.setBounds(320, 80, 120, 23);

        setSize(new java.awt.Dimension(791, 546));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
    }//GEN-LAST:event_formWindowOpened

    private void btnTemporizadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTemporizadorActionPerformed
        if (!tiempo.isRunning()) {
            btnTemporizador.setText("Detener");
            tiempo.start();
        } else {
            btnTemporizador.setText("Iniciar");
            tiempo.stop();
        }
    }//GEN-LAST:event_btnTemporizadorActionPerformed

    private void sldVelocidadStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sldVelocidadStateChanged
        delayTimer = sldVelocidad.getMaximum() - sldVelocidad.getValue();
        if (tiempo != null) {
            tiempo.setDelay(delayTimer);
        }
        lblVelocidad.setText("Velocidad: " + sldVelocidad.getValue());
    }//GEN-LAST:event_sldVelocidadStateChanged

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        agregarEsfera(evt.getX(), evt.getY(), sldAngulo.getValue());
    }//GEN-LAST:event_formMouseClicked

    private void sldAnguloStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sldAnguloStateChanged
        lblAngulo.setText("Ángulo: " + sldAngulo.getValue());
    }//GEN-LAST:event_sldAnguloStateChanged

    private void pnlContainerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pnlContainerMouseClicked
        agregarEsfera(evt.getX(), evt.getY(), sldAngulo.getValue());
    }//GEN-LAST:event_pnlContainerMouseClicked

    private void agregarEsfera(int x, int y, int angulo) {
        JLabel nuevo = new JLabel(Esfera.imagen);
        //Visualizar el Borde del Label
        //nuevo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));        
        pnlContainer.add(nuevo, 0);
        nuevo.setBounds(x - lblEsfera.getWidth() / 2, y - lblEsfera.getHeight() / 2,
                lblEsfera.getWidth(), lblEsfera.getHeight());
        agregarEsfera(nuevo, angulo);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new FrmPrincipal().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnTemporizador;
    private javax.swing.JCheckBox chkChocar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblAngulo;
    private javax.swing.JLabel lblAyuda;
    private javax.swing.JLabel lblC;
    private javax.swing.JLabel lblEsfera;
    private javax.swing.JLabel lblVelocidad;
    private javax.swing.JPanel pnlContainer;
    private javax.swing.JSeparator sepInterseccion;
    private javax.swing.JSlider sldAngulo;
    private javax.swing.JSlider sldVelocidad;
    // End of variables declaration//GEN-END:variables

}
