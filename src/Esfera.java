
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * @author BS
 */
public class Esfera{

    public static ImageIcon imagen = null;//Imagen para usar como icono

    //y=  mx+c
    //x= (y-c)/m
    private int mov = 1;//Cantidad de pixeles para el desplazamiento en el eje habilidado por cada movimiento de la esfera
    private double m = 1;//Pendiente
    private int c = 0;//Interseccion de la ecuación de la recta
    private int x = 0;//Coordenadas de movimiento en X basado en su ecuación de la recta
    private int y = 0;//Coordenadas de movimiento en Y basado en su ecuación de la recta
    private double angulo = 45;
    boolean movimientoConRespectoX = true;//Habilita con true que el movimiento será dependiente del X, y con false que será dependiente de y

    /////////////
    JLabel controlVisual = null;//Control visual que representa la esfera
    double anguloImagen = 0; //Angulo que está siendo utilizado para Dibujar la Imagen
    Esfera ultimoGolpe = null;

    public Esfera(JLabel control) {
        this.controlVisual = control;
        
    }
    
   /**
     *
     */
    public void sigImgRotada() {
        //Se obtiene el tamaño de la imagen que se requiere rotar
        int w = imagen.getIconWidth();
        int h = imagen.getIconHeight();

        //Se crea un lienzo para dibujar la imagen
        BufferedImage image = new BufferedImage(h, w, BufferedImage.TYPE_INT_ARGB_PRE);

        //Se obtiene el objeto grafico que tiene las funciones para dibujar
        Graphics2D g2d = image.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        //Se da un incremental del Angulo que se requiere manejar en todas las imagenes
        anguloImagen += 0.1;
        if (anguloImagen >= 360) {
            anguloImagen = anguloImagen - 360;
        }
        //Se aplica la rotación de la imagen
        g2d.rotate(anguloImagen, w / 2, h / 2);
        //Se dibuja la imagen
        g2d.drawImage(imagen.getImage(), 0, 0, null);
        //Se asigna la imagen al control de referencia
        controlVisual.setIcon(new ImageIcon(image));
    }

    public double getAngulo() {
        return angulo;
    }

    
    public void setAngulo(double angulo) {
        this.angulo = angulo;
        detectarDependenciaDelEje();
    }
    
    
    
    
    /**
     * Se recalcula cual es el angulo del movimiento de la esfera basado en su pendiente
     */
    public void recalcularAngulo() {
        setAngulo(Math.atan(getM()) * 180 / Math.PI);
    }

    /**
     * Se recalcula cual es la pendiente basado en el angulo
     */
    public void recalcularPendiente(){
        //La función Tangente requiere el valor del angulo en radianes
        //por lo cual se hace la conversión previa
        setM(Math.tan(getAngulo()*Math.PI/180));
    }
    
    /**
     * Recalcula el valor de la interseccion basado en los valores
     * actuales de m, x, y
     */
    public void recalcularInterseccion(){
        c = (int)(getY()-getM()*getX());        
    }
    /**
     * Realiza los nuevos calculos necesarios para el correcto funcionamiento de
     * la ecuacion de la recta teniendo en cuenta la posicion del Control
     */
    public void recalcularAPartirDeControl() {
        //y=mx+c
        //Se necesita la Intersección con el eje Y, se calcula el valor de "c"
        //c=y-mx
        //x es la Posicion del Objeto
        x = controlVisual.getX() + controlVisual.getWidth() / 2;
        y = controlVisual.getY() + controlVisual.getHeight() /2;
        recalcularInterseccion();
    }

    public void posicionarControl(){
        if (controlVisual!=null){
            //Centra el Control Basado en las Coordenadas Calculadas
            controlVisual.setBounds(getX() - controlVisual.getWidth() / 2, 
                    getY() - controlVisual.getHeight()/2, 
                    controlVisual.getWidth(), controlVisual.getHeight()
            );
        }
    }
    
    /**
     * Calcula la Siguiente posición que podria tener el Objeto, pero no la aplica
     * @return Objeto Point con las coordenadas.
     */
    public Point calcularSiguientePosicion(){
        int x;
        int y;
        if (this.movimientoConRespectoX){
            //y=mx+c
            x=this.getX()+this.mov;
            y=(int)(this.getM()*x+this.getC());
        }else{
            //x=(y-c)/m
            y=this.getY()+this.mov;
            x=(int)((y-this.getC())/this.getM());
        }
        return new Point(x, y);
    }
    
    public void asignarPosicion(Point posicion){
        this.x=posicion.x;
        this.y=posicion.y;
        posicionarControl();
    }
    
    /**
     * Asigna las coordenadas de la Esfera basado en la posición del control
     * @param posicion Objeto que representa la posición del JLabel
     */
    public void asignarPosicionControl(Point posicion){
        this.x=posicion.x+(controlVisual.getWidth()/2);
        this.y=posicion.y+(controlVisual.getHeight()/2);
        posicionarControl();
    }

    /**
     * Asigna las coordenadas actuales a partir del Control de referencia
     */
    public void asignarPosicionAPartirDeControl(){
        this.x=controlVisual.getX()+controlVisual.getWidth()/2;
        this.y=controlVisual.getY()+controlVisual.getHeight()/2;
        posicionarControl();
        recalcularPendiente();
        recalcularInterseccion();
    }
    
    /**
     * Cambia a un Movimiento Negativo
     */
    public void cambiarADireccionNegativa(){
        this.mov=Math.abs(this.mov)*-1;
    }
    
    /**
     * Cambia a un Movimiento Positivo
     */
    public void cambiarADireccionPositiva(){
        this.mov=Math.abs(this.mov);
    }
    
    /*
     * Si se ha detectado que existe un choque, realiza los ajustes
     * correspondientes teniendo en cuenta que se debe cambiar la direccion en
     * el eje x
     */
    public void golpeEnX() {
        if (movimientoConRespectoX) {
            mov *= -1;
        }
        setM(m * -1);
        recalcularAngulo();
        recalcularInterseccion();        
    }
    
    /*
     * Si se ha detectado que existe un choque, realiza los ajustes
     * correspondientes teniendo en cuenta que se debe cambiar la direccion en
     * el eje y
     */
    public void golpeEnY() {
        if (movimientoConRespectoX == false) {
            mov *= -1;
        }
        setM(m * -1);
        recalcularAngulo();
        recalcularInterseccion();
    }

    /**
     * 
     * @param esfera
     * @return Verdadero si la esfera tiene un impacto
     */
    public boolean hayImpacto(Esfera esfera){
        return obtenerDistancia(new Point(this.x, this.y), esfera);
    }
    
    public boolean hayImpacto(Point siguentePos, Esfera esfera){
        return obtenerDistancia(siguentePos, esfera);
    }
    
    
    public boolean obtenerDistancia(Point siguientePos, Esfera esfera){
        //Se calcula la distancia entre las dos esferas utilizando
        //el teorema de pitagoras: distancia=Raiz( (x2-x1)^2 + (y2-y1)^2 )
        double distancia=Math.sqrt(
                Math.pow(siguientePos.x-esfera.x, 2)
                +Math.pow(siguientePos.y-esfera.y, 2)
        );
        return distancia<=(this.controlVisual.getWidth()/2+esfera.controlVisual.getWidth()/2);
        
    }
    
    /**
     * 
     * @param esfera
     * @return El angulo existente entre dos esferas
     */
    public double anguloDeImpacto(Esfera esfera){
        double pendiente;
        ///Para Evitar un Error en el calculo de la pendiente se va a colocar
        ///que la diferencia en x es 1
        if (this.x-esfera.x==0){
            pendiente=(double)((this.y-esfera.y)/1);
        }else{
            pendiente=(double)((this.y-esfera.y)/(this.x-esfera.x));
        }
        //Convertir Radianes a Grados
        return Math.abs(Math.atan(pendiente)) * 180 / Math.PI;
    }
    
    
    
    /**
     * Se especifica que la ecuación de la recta será dependiente de X cuando el Angulo es <=45.
     * En caso contrario la ecuación de la recta será dependiente de Y
     */
    public void detectarDependenciaDelEje(){
        this.movimientoConRespectoX = (Math.abs(getAngulo())%90) <= 45;
    }
        
    public boolean estaEnTopeDerecho(int anchoArea){
        return this.getX()>anchoArea-(controlVisual.getWidth()/2);
    }
    
    public void asignarPuntoDerecho(int anchoArea){
        this.x=anchoArea-(controlVisual.getWidth()/2);
        posicionarControl();
    }
    
    public boolean estaEnTopeInferior(int altoArea){
        return this.getY()>altoArea-(controlVisual.getHeight()/2);
    }
    
    public void asignarPuntoInferior(int altoArea){
        this.y=altoArea-(controlVisual.getHeight()/2);
        posicionarControl();
    }
    
    public boolean estaEnTopeIzquierdo(){
        return this.getX()<controlVisual.getWidth()/2;
    }

    public void asignarPuntoIzquierdo(){
        this.x=controlVisual.getWidth()/2;
        posicionarControl();
    }
    
    public boolean estaEnTopeSuperior(){
        return this.getY()<controlVisual.getHeight()/2;
    }
    public void asignarTopeSuperior(){
        this.y=controlVisual.getHeight()/2;
        posicionarControl();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Esfera) {
            if (((Esfera) obj).controlVisual == controlVisual) {
                return true;
            }
        } else if (obj instanceof JLabel) {
            return obj == controlVisual;
        }
        return false;
    }

    @Override
    public int hashCode() {
        //El hascode del objeto será el mismo del control visual
        int hash = (this.controlVisual != null ? this.controlVisual.hashCode() : 0);
        return hash;
    }

    public double getM() {
        return m;
    }

    public int getC() {
        return c;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    public String obtenerEcuacion(){
        return y+"="+m+"*"+x+"+"+c;
    }

    public void setM(double m) {
        this.m = m;
        recalcularAngulo();
    }
}
